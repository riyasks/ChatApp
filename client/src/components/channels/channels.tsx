import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Chats, Employee } from '../../models/users';
import { AppEnum } from '../../enums/app.enums';
import { UserProps } from '../../models/props';


@inject('UserStore')
@observer
export class Channels extends Component<UserProps, {}> {
    usersList: Employee[];
    bookBorderstyle = {}
    constructor(props) {
        super(props);
    }
    generateGuid() {
        var result, i, j;
        result = '';
        for(j=0; j<32; j++) {
          if( j == 8 || j == 12 || j == 16 || j == 20) 
            result = result + '-';
          i = Math.floor(Math.random()*16).toString(16).toUpperCase();
          result = result + i;
        }
        return result;
      }
    render() {
        return (
            <Fragment>
                <ul className="list-unstyled friend-list scrollable">
                    {
                        this.props.UserStore.channelList.length > 0 ? this.props.UserStore.channelList.map(channel => (
                            <li key={channel.groupName+this.generateGuid()} className="active grey lighten-3 p-2 point" onClick={() => {
                                this.props.UserStore.SelectGroup(channel)
                            }}>
                                <a className="d-flex">
                                    {/* {
                                        friend.status === "Offline" ? <span className="indicator offline"></span> : <span className="indicator online"></span>
                                    } */}
                                    <img src="https://www.pinclipart.com/picdir/middle/124-1244891_user-groups-filled-icon-group-icon-png-clipart.png" alt="avatar" className="avatar rounded-circle d-flex align-self-center mr-2 z-depth-1" />
                                    <div className="text-small">
                                        <strong>{channel.groupName}</strong> : ({channel.members.length} Members)
                                        <p className="last-message text-muted">{channel.groupDescription}</p>
                                    </div>
                                    {
                                        <span className="badge">{channel.unreadCount === 0 ? "" : channel.unreadCount}</span> 
                                    }

                                </a>
                            </li>
                        )) : "No Channels"
                    }
                </ul>

            </Fragment>
        )
    }

}


