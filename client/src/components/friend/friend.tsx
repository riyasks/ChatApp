import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Chats, Employee } from '../../models/users';
import { AppEnum } from '../../enums/app.enums';
import { UserProps } from '../../models/props';


@inject('UserStore')
@observer
export class Friends extends Component<UserProps, {}> {
    usersList: Employee[];
    bookBorderstyle = {}
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <ul className="list-unstyled friend-list scrollable">
                    {
                        this.props.UserStore.usersList.length > 0 ? this.props.UserStore.usersList.map(friend => (
                            <li key={friend.email} className="active grey lighten-3 p-2 point" onClick={() => {
                                this.props.UserStore.SelectedUser(friend)
                            }}>
                                <a className="d-flex">
                                    {
                                        friend.status === "Offline" ? <span className="indicator offline"></span> : <span className="indicator online"></span>
                                    }
                                    <img src={friend.avatar} alt="avatar" className="avatar rounded-circle d-flex align-self-center mr-2 z-depth-1" />
                                    <div className="text-small">
                                        <strong>{friend.firstName}</strong>
                                        <p className="last-message text-muted">{friend.email}</p>
                                    </div>
                                    {
                                        <span className="badge">{friend.unreadCount === 0 ? "" : friend.unreadCount}</span> 
                                    }

                                </a>
                            </li>
                        )) : "No Data"
                    }
                </ul>

            </Fragment>
        )
    }

}


