import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Chats, Employee } from '../../models/users';
import { Friends } from '../friend/friend';
import { Channels } from '../channels/channels';
import { Chat } from '../chats/chat';
import { UserProps } from '../../models/props';
import { SignalRService } from '../../store/signalr';

@inject('UserStore')
@observer
export class Dashboard extends Component<UserProps, {}> {
    usersList: Employee[];
    bookBorderstyle = {}
    connection: signalR.HubConnectionBuilder;
    constructor(props) {
        super(props);
    }
    async GetALLUsers() {
      
        await this.props.UserStore.GetAllUsers();
        await this.props.UserStore.GetAllGroups();
       
      
    }
inVokeListeners(Signal){
    Signal.hubConnection.on("setStatus", data => {
        debugger
        var userId = data.userId;
        var users = this.props.UserStore.usersList;
        var index = this.props.UserStore.usersList.findIndex((u) => { return u.userId == userId })
        if(index!=-1){
        users[index].status = data.status;
        users[index].lastOnline = data.lastseen;
        }
    });
    Signal.hubConnection.on("SentMessage", data => {
        debugger
        if (this.props.UserStore.selectedEmployee.firstName == undefined) {
            var userId = data.userId;
            var users = this.props.UserStore.usersList;
            var index = this.props.UserStore.usersList.findIndex((u) => { return u.userId == userId })
            if(index!=-1){
                users[index]["unreadCount"] = users[index]["unreadCount"] + 1
                }
           
        }
        else {
            
            if (this.props.UserStore.selectedEmployee.userId == data.userId) {
                //selected groupid
                //loginuserID
                debugger
                var userId=JSON.parse(localStorage.getItem('employee')).userId;
                this.props.UserStore.pushChat(data);
                this.props.UserStore.UpdateReadMessage(userId)
            }
            else {

                var emp = JSON.parse(localStorage.getItem('employee'));
                if (emp.userId == data.userId) {
                    this.props.UserStore.pushChat(data);
                }
                else {
                    var userId = data.userId;
                    var users = this.props.UserStore.usersList;
                    var index = this.props.UserStore.usersList.findIndex((u) => { return u.userId == userId })
                    if(index!=-1){
                        users[index]["unreadCount"] = users[index]["unreadCount"] + 1
                        }
                    
                }
            }

        }

        var myDiv = document.getElementById("messagesDiv");
        myDiv.scrollTop = myDiv.scrollHeight;

    });

    Signal.hubConnection.on("NewGroupNotification", data => {
        debugger
        this.props.UserStore.pushChannel(data);
   
        alert("You have being added to the channel "+data.groupName)
      

    });
    Signal.hubConnection.on("NewChannelMessage", (data) => {
        debugger
        var groupId = data.group.groupId;
       if(this.props.UserStore.selectedChannel.groupId==undefined){
       
        //    this.props.UserStore.channelList;
     
        var channels = this.props.UserStore.channelList;
        var index = this.props.UserStore.channelList.findIndex((u) => { return u.groupId == groupId })
        if(index!=-1){
            channels[index]["unreadCount"] = channels[index]["unreadCount"] + 1
            }
            
       this.props.UserStore.pushChat(data.message)
       var myDiv = document.getElementById("messagesDiv");
       myDiv.scrollTop = myDiv.scrollHeight;
       }
       else{

            if(this.props.UserStore.selectedChannel.groupId==groupId){
                this.props.UserStore.pushChat(data.message)
                var userId=JSON.parse(localStorage.getItem('employee')).userId;
                this.props.UserStore.UpdateReadMessageChannel(userId)
                var myDiv = document.getElementById("messagesDiv");
                myDiv.scrollTop = myDiv.scrollHeight;
            }
            else{
                var channels = this.props.UserStore.channelList;
        var index = this.props.UserStore.channelList.findIndex((u) => { return u.groupId == groupId })
        if(index!=-1){
            channels[index]["unreadCount"] = channels[index]["unreadCount"] + 1
            }
            }


       }
       

   
       
      

    });
    
}
    componentDidMount() {
        this.GetALLUsers();
        var user = JSON.parse(localStorage.getItem('employee'))
        if (user) {
            this.props.UserStore.SetUser(user);
            var Signal = new SignalRService("Online");
            if (Signal.hubConnection) {
             //Connection Available
             this.inVokeListeners(Signal)
            }
            else {
                Signal.Init((callback) => {
                    if (callback) {
                       
                        this.inVokeListeners(Signal)

                    }

                });

            }



        }

    }

    render() {
    
        return (
            <Fragment>
                <div className="card grey lighten-3 chat-room">
                    {/* <div className="card-body"> */}
                        <div className="row px-lg-2 px-2">
                            <div className="col-md-3 col-xl-3 px-0">
                                <div className="white z-depth-1 px-3 pt-3 pb-0">
                                    <Friends />
                                </div>
                            </div>
                            <div className="col-md-6 col-xl-6 pl-md-3 px-lg-auto px-0">
                            <div className="white z-depth-1 px-3 pt-3 pb-0">
                            <Chat/>
                            </div>
                            </div>
                           
                            <div className="col-md-3 col-xl-3 px-0">
                                <div className="white z-depth-1 px-3 pt-3 pb-0">
                                    <Channels />
                                </div>
                            </div>
                        </div>
                    {/* </div> */}
                </div>

            </Fragment>
        )
    }
}

