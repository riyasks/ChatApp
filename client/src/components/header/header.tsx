
import {
  MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse

} from "mdbreact";


import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import Auth from '../../models/auth';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Multiselect } from 'multiselect-react-dropdown';
import { UserStore } from '../../store/users.store';
import { SignalRService } from "../../store/signalr";
@inject('UserStore')
@observer
export class AppHeader extends Component<IHeaderProps, IHeaderState> {
  Selectedusers: any[];


  constructor(props) {
    super(props);
   this.Selectedusers=[];
    
    debugger
    this.state = {
      options:[],
      selectedValue:[],
      groupName:"",
      groupDescription:""
    }


  }

  onSelect=(selectedList, selectedItem) =>{
    debugger
   this.setState({
     selectedValue:selectedList
   })
}
 
onRemove=(selectedList, removedItem) =>{
  this.setState({
    selectedValue:selectedList
  })
 
}

componentDidMount(){
 
}

  render() {

    return (

      <Fragment>
        {/* {this.props.UserStore.showHeader} */}
        {this.props.UserStore.User?.userId ?
          <MDBNavbar className="sticky-top" color="black" dark expand="md">
            <MDBNavbarBrand>
              <img className="avatar rounded-circle d-flex align-self-center mr-2 z-depth-1" src={JSON.parse(localStorage.getItem('employee'))?.avatar } style={{width:'45px'}}/>
              <strong className="white-text">Hello {this.props.UserStore.User.firstName}</strong>
            </MDBNavbarBrand>
            <MDBNavbarToggler onClick={() => { }} />
            <MDBCollapse id="navbarCollapse3" navbar>
              <MDBNavbarNav left>

              </MDBNavbarNav>
              <MDBNavbarNav right>
              {this.props.UserStore.usersList.length>0?  <Fragment><button type="button" className="btn btn-danger" onClick={()=>{
                  document.getElementById('AddgroupFormBtn').click()
                }}>Add Groups</button> <div className="clearfix">...</div></Fragment>:""} 
               
                <button type="button" className="btn btn-danger" onClick={() => {
                  var user = JSON.parse(localStorage.getItem('employee'))

                 var Signal = new SignalRService("Offline");
                
                  this.props.UserStore.SetHeader(false);
                  this.props.UserStore.SetStatus(user.userId, "Offline");


                  this.props.UserStore.ClearAllObservants()

                  localStorage.clear();
                  this.props.history.push("/login")
                }}>Logout</button>

              </MDBNavbarNav>
            </MDBCollapse>
          </MDBNavbar>
          : ""}
        
<button type="button" style={{display:'none'}} id="AddgroupFormBtn" className="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>


<div id="myModal" className="modal fade" role="dialog">
  <div className="modal-dialog">

    <div className="modal-content">
      <div className="modal-header">
        <h4 className="modal-title">Create Groups</h4>
      </div>
      <div className="modal-body">
  


 
  

   
    <form>
      
      <div className="md-form">
        <input type="text" id="materialFormCardNameEx" onChange={(e)=>{
          this.setState({
            groupName:e.target.value
          })
        }} placeholder="group name" className="form-control"/>
        <label htmlFor="materialFormCardNameEx" className="font-weight-light"></label>
      </div>

    
      <div className="md-form">
        <input type="textarea" id="materialFormCardEmailEx" onChange={(e)=>{
          this.setState({
            groupDescription:e.target.value
          })
        }} placeholder="group description" className="form-control"/>
        <label htmlFor="materialFormCardEmailEx" className="font-weight-light"></label>
      </div>

    
      <div className="md-form">
      <Multiselect
         options={this.props.UserStore.usersListDropdown} // Options to display in the dropdown
          selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
          onSelect={this.onSelect} // Function will trigger on select event
          onRemove={this.onRemove} // Function will trigger on remove event
          displayValue="name" // Property name to display in the dropdown options
      />
        <label htmlFor="materialFormCardConfirmEx" className="font-weight-light"></label>
      </div>

    
    

   
    </form>
   

 



      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        <button className="btn btn-primary" type="button" onClick={()=>{
          debugger
          if(this.state.selectedValue.length==0|| this.state.groupName==""){
            alert("validation failed");
          }
          else{
            var a=this.state.selectedValue
            this.props.UserStore.CreateChannel(this.state);
            document.getElementById('AddgroupFormBtn').click()
          }
         
        }}>Create Group</button>
      </div>
    </div>

  </div>
</div>
      </Fragment>
    )
  }
}


interface IHeaderProps extends RouteComponentProps<{}> {
  UserStore: UserStore
}
interface IHeaderState  {
  options: any,
  selectedValue:any,
  groupName:string,
  groupDescription:string

}
export default withRouter(AppHeader);