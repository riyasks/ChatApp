import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import Auth from '../../models/auth';
import { withRouter, Redirect, RouteComponentProps } from 'react-router-dom';
import { UserStore } from '../../store/users.store';
import { SignalRService } from '../../store/signalr';
@inject('UserStore')
@observer
export class Login extends Component<ILoginProps, Auth> {

    constructor(props) {
        super(props);
        this.state = new Auth();
    }



    render() {

        return (
            <Fragment>
       <div className="container">
        <h2>Dirico</h2>
        <br />
        <div className="wrapper">
          {/* <p className="p">Email</p> */}
          <div className="md-form">
                                <input type="email" placeholder="email" id="materialLoginFormEmail" onChange={(e) => {
                                    this.setState({
                                        username: e.target.value
                                    })
                                }} className="form-control" />
                            </div>
          <br />
          {/* <p className="p">Password</p> */}
          <div className="md-form">
                                <input type="password" placeholder="Password" id="materialLoginFormPassword" onChange={(e) => {
                                    this.setState({
                                        password: e.target.value
                                    })
                                }} className="form-control" />
                            </div>
          <br /><br />
          <button className="btn btn-primary" onClick={async () => {
                                var data = await this.props.UserStore.Login(this.state);
                                if (data) {
                                    var user = JSON.parse(localStorage.getItem('employee'))
                                    if (user) {
                                       
                                        this.props.UserStore.SetUser(user);
                                        var Signal = new SignalRService("Online");
                                        if (Signal.hubConnection) {
                                            // Signal.hubConnection.on("setStatus", data => {
                                            //     var userId = data.userId;
                                            //     var users = this.props.UserStore.usersList;
                                            //     var index = this.props.UserStore.usersList.findIndex((u) => { return u.userId == userId })
                                            //     users[index].status = data.status;

                                            // });
                                           
                                        }
                                        else {
                                            Signal.Init((callback) => {
                                                if (callback) {
                                                    this.props.UserStore.SetStatus(user.userId, "Online");
                                                    Signal.hubConnection.on("setStatus", data => {
                                                        var userId = data.userId;

                                                        var users = this.props.UserStore.usersList;
                                                        var index = this.props.UserStore.usersList.findIndex((u) => { return u.userId == userId })
                                                        users[index].status = data.status;
                                                        Signal.hubConnection.invoke("sendConnectionId", Signal.hubConnection.connectionId,user.userId,"Online");
                                                //        alert(localStorage.getItem('connectionId'))

                                                       // this.props.UserStore.SetStatus(userId, data.status,localStorage.getItem('connectionId'));
                                                    });
                                                }
                                            });
                                        }
                                    }
                                    this.props.history.push("/dashboard")
                                }
                            }} >Login</button><br />
        </div>
      </div>

            </Fragment>
        )
    }
}


interface ILoginProps extends RouteComponentProps<{}> {
    UserStore: UserStore
}

export default withRouter(Login);