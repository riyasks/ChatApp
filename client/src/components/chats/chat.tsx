import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Chats, Employee } from '../../models/users';
import { UserProps } from '../../models/props';
import Moment from 'react-moment';

@inject('UserStore')
@observer
export class Chat extends Component<UserProps, Chats> {
    constructor(props){
      
        super(props);
        this.state = {
            input: ""
        }
    }
    getTimeAgo(){
        debugger
        var time=this.props.UserStore.selectedEmployee.lastOnline;
    return   <Moment fromNow ago>{time}</Moment>

    }
     generateGuid() {
        var result, i, j;
        result = '';
        for(j=0; j<32; j++) {
          if( j == 8 || j == 12 || j == 16 || j == 20) 
            result = result + '-';
          i = Math.floor(Math.random()*16).toString(16).toUpperCase();
          result = result + i;
        }
        return result;
      }
    render() {
        const individualChat= this.props.UserStore.selectedEmployee.firstName != undefined ? <Fragment>
          
        <div className="row headersticky sticky-top navbar-dark black navbar navbar-expand-md sticky-top">
            <img src={this.props.UserStore.selectedEmployee.avatar} alt="avatar" className="avatar rounded-circle mr-2 ml-lg-3 ml-0 z-depth-1" />
            <span>{this.props.UserStore.selectedEmployee.firstName} : {this.props.UserStore.selectedEmployee.status === "Online" ? 'Online' : this.props.UserStore.selectedEmployee.lastOnline ?  this.getTimeAgo() : "Not online yet"}</span>
        </div>
        <hr></hr>
        <div className="chat-message">
            <ul className="list-unstyled chat scrollable-chat" id="messagesDiv">
                {
                    this.props.UserStore.chats.length > 0 ? this.props.UserStore.chats.map(message => (
                        <li key={message.text+this.generateGuid()} className="d-flex mb-2">
                            <div className="" style={{ width: '100%', textAlign: 'left' }}>
                                <strong className="primary-font">{message.userName === JSON.parse(localStorage.getItem('employee')).firstName ? "You" : message.userName}</strong>
                                <p className="mb-0">
                                    {message.text}
                                </p>
                            </div>
                        </li>
                    )) : ""
                }
            </ul>
            <div className="form-group basic-textarea white">
                <textarea value={this.state.input} onChange={(e) => {
                    this.setState({
                        input: e.target.value
                    })
                }} className="form-control pl-2 my-0" id="exampleFormControlTextarea2" rows={3} placeholder="Type your message here..." />
            </div>
            <button type="button" onClick={() => {
                this.props.UserStore.sendMessage(this.state.input)
                this.setState({
                    input: ""
                })
            }} className="btn btn-info btn-rounded btn-sm waves-effect waves-light float-right">Send</button>
        </div>
   
</Fragment> : ""
  const ChannelChat= this.props.UserStore.selectedChannel.groupName != undefined ? <Fragment>
          
  <div className="row headersticky sticky-top navbar-dark black navbar navbar-expand-md sticky-top">
      <img src="https://www.pinclipart.com/picdir/middle/124-1244891_user-groups-filled-icon-group-icon-png-clipart.png" alt="avatar" className="avatar rounded-circle mr-2 ml-lg-3 ml-0 z-depth-1" />
      <span>{this.props.UserStore.selectedChannel.groupName} </span>
  </div>
  <hr></hr>
  <div className="chat-message">
      <ul className="list-unstyled chat scrollable-chat" id="messagesDiv">
          {
              this.props.UserStore.chats.length > 0 ? this.props.UserStore.chats.map(message => (
                  <li key={message.text+this.generateGuid()} className="d-flex mb-2">
                      <div className="" style={{ width: '100%', textAlign: 'left' }}>
                          <strong className="primary-font">{message.userName === JSON.parse(localStorage.getItem('employee')).firstName ? "You" : message.userName}</strong>
                          <p className="mb-0">
                              {message.text}
                          </p>
                      </div>
                  </li>
              )) : ""
          }
      </ul>
      <div className="form-group basic-textarea white">
          <textarea value={this.state.input} onChange={(e) => {
              this.setState({
                  input: e.target.value
              })
          }} className="form-control pl-2 my-0" id="exampleFormControlTextarea2" rows={3} placeholder="Type your message here..." />
      </div>
      <button type="button" onClick={() => {
          this.props.UserStore.sendChannelMessage(this.state.input)
          this.setState({
              input: ""
          })
      }} className="btn btn-info btn-rounded btn-sm waves-effect waves-light float-right">Send</button>
  </div>

</Fragment> : ""
        return (
            <Fragment>
                {this.props.UserStore.SelectedChatType=="Channel"? ChannelChat:individualChat}
            
            </Fragment>
       
        
        )
    }

}


