

    export interface Name {
        title: string;
        first: string;
        last: string;
    }

    export interface Location {
        street: string;
        city: string;
        state: string;
        zip: number;
    }

    export interface Picture {
        large: string;
        medium: string;
        thumbnail: string;
    }

    // export class User {
    //     gender: string;
    //     name: Name;
    //     location: Location;
    //     email: string;
    //     username: string;
    //     password: string;
    //     salt: string;
    //     md5: string;
    //     sha1: string;
    //     sha256: string;
    //     registered: number;
    //     dob: number;
    //     phone: string;
    //     cell: string;
    //     picture: Picture;
    // }

    // export interface Result {
    //     user: User;
    // }

    // export interface ResponseApi {
    //     results: Result[];
    //     // nationality: string;
    //     // seed: string;
    //     // version: string;
    // }



    export class Chat{
        name:string
        avatar: string
        message: string
        when: string
        toRespond: number
        seen: boolean
        active: boolean
       
    }

    export class Chats{
     //   chats: Chat[];
      //  messages: Message[];
        input:string=""
    }

    export class Message{
        author: string
        avatar: string
        when: string
        message:string
    }




    export class Employee{
        userId:string;
        firstName:string
        lastName:string
        phone:string
        email:string
        passWord:string
        status:string;
        absenseNote:string;
        avatar:string;
        groups:any;
        // badge:number=0
        lastOnline:string;
        unreadCount:number=0

    }

    
    export class Messages{
        text:string;
        userId:string
        // lastName:string
        readBy:string[]
        modified:string
        avatar:string;
        userName:string;
       
    }
      
    export class Group{
        groupId:string;
        groupName:string
        // lastName:string
        members:string[]
        chats:Messages[]
        groupDescription:string;
        createdBy:string;
        modified:any;
        unreadCount:number=0
        avatar:string
       
    }


