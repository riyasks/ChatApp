import 'tslib'
import "./styles/app.css"
import "./styles/style.css"
import "./styles/login.css"
import React from 'react'
import ReactDOM from 'react-dom'
import 'mobx-react-lite/batchingForReactDom'
import { App } from './app'
// import { SignalRService } from './store/signalr'
// import '@fortawesome/fontawesome-free/css/all.min.css';
// import 'bootstrap-css-only/css/bootstrap.min.css';
// import 'mdbreact/dist/css/mdb.css';
const root = document.getElementById('app-root')
// var Signal=new SignalRService();
// Signal.Init()
ReactDOM.render(<App />, root)
