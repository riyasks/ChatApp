import React, { Fragment } from 'react'
import { Component } from 'react'
import { Provider } from 'mobx-react'
import { Dashboard } from './components/dashboard/dashboard'
import { UserStore } from './store/users.store'
import AppHeader from './components/header/header'
import { Login } from "./components/login/login";
import { HashRouter as Router, Route, Link, Switch, BrowserRouter,useHistory, useLocation  } from 'react-router-dom'


export class App extends Component<{}, {}> {
  //Inject the userstore 
  private UserStore: UserStore = new UserStore()


  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: ""
    }

  }

  componentDidMount() {

  }

  findRoute=()=>{
    if(localStorage.getItem('employee')){
    
      return Dashboard;
    }
    else{
      return Login;
    }
    //return  UsersList
  }


  render() {
    return (
      <Provider UserStore={this.UserStore}>
        <BrowserRouter>
      {<AppHeader UserStore={this.UserStore} /> }  
          <Switch>
            <Route exact path="/" component={
              this.findRoute()
            } />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/login" component={Login} />
          </Switch>
        </BrowserRouter>

      </Provider>
    )
  }
}
