import * as signalR from "@microsoft/signalr";
export class SignalRService{
    status:string;
    constructor(status:string){
        this.status=status;
    }
    hubConnection;
    Init(callback){
        debugger
        this.hubConnection = new signalR.HubConnectionBuilder()
        .withUrl("https://localhost:44306/chat")
      // .withAutomaticReconnect()
      .withAutomaticReconnect([0, 1000, 5000, null])
        //   .withUrl("https://localhost:44306/chat", {
        //     // skipNegotiation: true,
        //     // transport: signalR.HttpTransportType.WebSockets
        //    })
        .configureLogging(signalR.LogLevel.Information)  
        .build();
       
        // Starts the SignalR connection
        this.hubConnection.start().then(a => {
          // Once started, invokes the sendConnectionId in our ChatHub inside our ASP.NET Core application.
          if (this.hubConnection.connectionId) {
            var user=JSON.parse(localStorage.getItem('employee'));
            localStorage.setItem("connectionId",this.hubConnection.connectionId)
            if(user){
                debugger
                this.hubConnection.invoke("sendConnectionId", this.hubConnection.connectionId,user.userId,this.status);
                callback(true)
            }
          }   
        });  

       this.hubConnection.onclose(async () => {
          await this.hubConnection.start().then(a => {
            // Once started, invokes the sendConnectionId in our ChatHub inside our ASP.NET Core application.
            if (this.hubConnection.connectionId) {
              var user=JSON.parse(localStorage.getItem('employee'));
              localStorage.setItem("connectionId",this.hubConnection.connectionId)
              if(user){
                  debugger
                  this.hubConnection.invoke("sendConnectionId", this.hubConnection.connectionId,user.userId,this.status);
                  callback(true)
              }
            }   
          });
      });
    }
}