import { observable, action } from 'mobx'
import { Employee, Messages, Group } from '../models/users';
import { AppEnum } from '../enums/app.enums';
import Auth from '../models/auth';



export class UserStore {
  @observable usersList: Employee[] = [];
  @observable channelList: Group[] = [];
  @observable selectedEmployee: Employee=new Employee()
  @observable selectedChannel: Group=new Group()
  @observable chats: Messages[] = [];
  @observable userAuth: Auth
  @observable showHeader: boolean=false;
  @observable User: Employee=new Employee()
  @observable SelectedGroup:Group=new Group();
  @observable usersListDropdown: any[] = [];
  @observable SelectedChatType:string;

  @action
  SetHeader(data:boolean){
    this.showHeader=data
  }

  @action
  SetUser(data:Employee){
    this.User=data
  }
  @action
  pushChat(message:Messages){
    debugger
    this.chats.push(message)
  
  }
  @action
  pushChannel(group:Group){
    this.channelList.push(group)
  }
  @action
  SetStatus(userId:string,status:string){
    let params = {
      "userId": userId,
      "status":status
 
    };
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/SetStatus?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then(async (data) => {
      }).catch((err) => {
        reject(err)
      })
    })
  }

  @action
  UpdateReadMessage(userId:string){
    let params = {
      "userId": userId,
      "groupId":this.SelectedGroup.groupId
    };
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/UpdateReadMessage?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then(async (data) => {
      }).catch((err) => {
        reject(err)
      })
    })
  }
  @action
  UpdateReadMessageChannel(userId:string){
    let params = {
      "userId": userId,
      "groupId":this.selectedChannel.groupId
    };
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/UpdateReadMessage?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then(async (data) => {
      }).catch((err) => {
        reject(err)
      })
    })
  }
  @action
  Login = async (Users: Auth)=> {
    this.userAuth = Users;
    let params = {
      "email": this.userAuth.username,
      "password": this.userAuth.password
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    
    let url = AppEnum.ApiUrl+'api/Employees/Login?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(async(response) =>{ 
       return await response.json()
      }).then(async (data) => {
        if(data.userId!=undefined){
          localStorage.setItem('employee',JSON.stringify(data));
          this.User=data;
          this.showHeader=true;
          // this.usersList=[]
          // setTimeout(() => {
          //   this.GetAllUsers()
          // }, 2000);
        
          resolve(data);
        }
      }).catch((err) => {
        reject(err)
        alert("UserName/Password not correct")
      })
    })
  }

  
  @action
  sendMessage = async (input:string)=> {
    var selectemp=this.selectedEmployee
    var me=JSON.parse(localStorage.getItem('employee'))
   let params = {
     "mymessage": input,
     "myuserid": me.userId,
     "theiruserId":selectemp.userId,
     "connectionId": localStorage.getItem('connectionId')
   };
   let query = Object.keys(params)
                .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                .join('&');
   let url = AppEnum.ApiUrl+'api/Employees/sendMessage?' + query;
   return new Promise((resolve, reject) => {
     fetch(url).then(response => response.json()).then(async (data) => {
      var myDiv = document.getElementById("messagesDiv");
      myDiv.scrollTop = myDiv.scrollHeight;
       resolve(data);
     }).catch((err) => {
       reject(err)
     })
   })
  }


  @action
  sendChannelMessage = async (input:string)=> {
    var selectchannel=this.selectedChannel
    var me=JSON.parse(localStorage.getItem('employee'))
   let params = {
     "mymessage": input,
     "myuserid": me.userId,
     "selectchannelId":selectchannel.groupId
    
   };
   let query = Object.keys(params)
                .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                .join('&');
   let url = AppEnum.ApiUrl+'api/Employees/sendChannelMessage?' + query;
   return new Promise((resolve, reject) => {
     fetch(url).then(response => response.json()).then(async (data) => {
      var myDiv = document.getElementById("messagesDiv");
      myDiv.scrollTop = myDiv.scrollHeight;
       resolve(data);
     }).catch((err) => {
       reject(err)
     })
   })
  }

  @action
  ClearAllObservants = async ()=> {
    this.usersList= [];
    this.selectedEmployee=new Employee()
    this.showHeader=false;
    this.chats = [];
    this.User=new Employee()
    this.SelectedGroup=new Group();
    this.selectedChannel=new Group();
    this.channelList=[];
    
  }
  @action
  GetAllUsers = async ()=> {
    let params = {
      "myUserID": JSON.parse(localStorage.getItem('employee')).userId
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/GetAllEmployees?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then((data) => {
        var userId=JSON.parse(localStorage.getItem('employee')).userId
        data.forEach(element => {
          if(element.userId!=userId){
            this.usersList.push(element)
          }
        });
        this.usersList.slice().sort(function(a, b){
          if(a.status == "Online") { return -1; }
          if(a.status == "Offline") { return 1; }
          return 0;
      })
      var currentuser=JSON.parse(localStorage.getItem('employee'));
        this.usersList=this.usersList.slice().reverse();
        for (let index = 0; index < this.usersList.length; index++) {
          const user = this.usersList[index];
          debugger
         
          if(user.userId!==currentuser.userId){
            this.usersListDropdown.push({
              id:user.userId,
              name:user.firstName
            })
          }
        
          
        }
        resolve(data);
      }).catch((err) => {
        reject(err)
      })
    })
  }

  @action
  GetAllGroups = async ()=> {
    let params = {
      "myUserID": JSON.parse(localStorage.getItem('employee')).userId
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/GetAllGroups?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then((data) => {
      
        data.forEach(element => {
        
            this.channelList.push(element)
         
        });
     

       
        resolve(data);
      }).catch((err) => {
        reject(err)
      })
    })
  }


  @action
  CreateChannel = async (state)=> {
    debugger
    var users=[]
    for (let index = 0; index < state.selectedValue.length; index++) {
      const user = state.selectedValue[index];
        users.push(user.id)
      
    }
    let params = {
      "groupName": state.groupName,
      "groupDescription":state.groupDescription,
      "createdBy":JSON.parse(localStorage.getItem('employee')).userId,
      "Selectedusers":JSON.stringify(users)
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    let url = AppEnum.ApiUrl+'api/Employees/CreateChannel?' + query;
    return new Promise((resolve, reject) => {
      fetch(url,).then(response => response.json()).then((data) => {
      debugger
        resolve(data);
      }).catch((err) => {
        reject(err)
      })
    })
  }


  @action
  SelectedUser = async (user:Employee)=> {
    debugger
    this.SelectedChatType="Individual";
    this.selectedEmployee=user;
    this.selectedEmployee.unreadCount=0;

    this.selectedChannel=new Group();
    

    let _this = this;
    this.chats=[];
    let params = {
      "theiruserId":user.userId,
      "myuserId": JSON.parse(localStorage.getItem('employee')).userId
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    
    let url = AppEnum.ApiUrl+'api/Employees/FindGroup?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then((data) => {
          debugger
         this.SelectedGroup=data;
        data.chats.forEach(element => {
          
          this.chats.push(element)
       
        });
        var myDiv = document.getElementById("messagesDiv");
        myDiv.scrollTop = myDiv.scrollHeight;

        resolve(data);
      }).catch((err) => {
        reject(err)
      })
    })
  }

  @action
  SelectGroup = async (group:Group)=> {
    debugger
    this.SelectedChatType="Channel"
    this.selectedChannel=group;
    this.selectedChannel.unreadCount=0;
    this.selectedEmployee=new Employee();
   
    this.chats=[];
    let params = {
      "groupId":group.groupId,
      "myuserId": JSON.parse(localStorage.getItem('employee')).userId
    };
    
    let query = Object.keys(params)
                 .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
                 .join('&');
    
    let url = AppEnum.ApiUrl+'api/Employees/FindGroupChannel?' + query;
    return new Promise((resolve, reject) => {
      fetch(url).then(response => response.json()).then((data) => {
          debugger
        // this.selectedChannel=data;
        if(data.chats){
          data.chats.forEach(element => {
          
            this.chats.push(element)
         
          });
          var myDiv = document.getElementById("messagesDiv");
          myDiv.scrollTop = myDiv.scrollHeight;
        }
        else{
          this.chats=[];
        }
       

        resolve(data);
      }).catch((err) => {
        reject(err)
      })
    })
  }

}

export const userStore = new UserStore()
