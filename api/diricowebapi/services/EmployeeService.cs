﻿using diricowebapi.Models;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;
using SignalRChat.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diricowebapi.services
{
    public class EmployeeService
    {
        private readonly IMongoCollection<Employee> _employees;
        private readonly IMongoCollection<Groups> _groups;
        private readonly IHubContext<ChatHub> _hubContext;
        public EmployeeService(EmployeeDatabaseSettings settings, IHubContext<ChatHub> hubContext)
        {
            this._hubContext = hubContext;
            var servers = new List<MongoServerAddress>() { new MongoServerAddress("ds055752.mongolab.com", 55752) };
            var credential = MongoCredential.CreateCredential("dirico", "riyasalim", "abc123*");
            var mongoClientSettings = new MongoClientSettings()
            {
                ConnectionMode = ConnectionMode.Direct,
                Credential = credential,
                Servers = servers.ToArray(),
                RetryWrites = false,
                ApplicationName = "dirico",
            };

            MongoClient client = new MongoClient(mongoClientSettings);
            var database = client.GetDatabase(settings.DatabaseName);
            _employees = database.GetCollection<Employee>(settings.EmployeesCollectionName);
            _groups = database.GetCollection<Groups>("groups");

        }

        public Groups UpdateReadMessage(string userId, string groupId)
        {

            //var filter1 = Builders<Groups>.Filter.Eq(x => x.GroupName, "");
            //var filter2 = Builders<Groups>.Filter.In(theiruserId, myuserId);
            var group = _groups.Find(x => x.GroupId==groupId).FirstOrDefault();
            if (group != null)
            {
                //Update read count

                if (group.Chats != null)
                {
                    var chats = group.Chats.ToList();

                    for (int i = 0; i < chats.Count(); i++)
                    {
                        if (!chats[i].ReadBy.Contains(userId))
                        {
                            chats[i].ReadBy.Add(userId);
                        }
                    }
                }
               
                
                //.ForEach(c => c.ReadBy.Contains(userId)? c.ReadBy.Add(userId));
                _groups.UpdateOne(x => x.GroupId == group.GroupId, Builders<Groups>.Update.Set(x => x.Chats, group.Chats));
                // var update = _groups.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.Status, status));
            }
            return group;
        }

        public async Task<Groups> sendChannelMessageAsync(string message, string myuserId,string groupId)
        {

         
            var group = _groups.Find(x => x.GroupId == groupId).FirstOrDefault();
            if (group != null)
            {
                //Update read count
                var me = _employees.Find(x => x.UserId == myuserId).FirstOrDefault();
                var _message = new Messages();
                var readbyuser = new List<string>();
                readbyuser.Add(myuserId);
                _message.ReadBy = readbyuser;
                _message.Text = message;
                _message.UserId = myuserId;
                _message.Avatar = me.Avatar;
                _message.UserName = me.FirstName;

                var members = group.Members;

                //var _ChannelMessage = new ChannelMessage();
                //_ChannelMessage.group = group;
                //_ChannelMessage.Message = _message;
                //await _hubContext.Clients.Client(me.ConnectionId).SendAsync("NewChannelMessage", _ChannelMessage);

                if (members.Count() > 0)
                {

                    await Task.WhenAll(members.Select((id) => DoChannelMessageDeliveryAsync(id, group, _message)));
                }

                if (group.Chats == null)
                {
                    var newMessages = new List<Messages>();
                    newMessages.Add(_message);
                    group.Chats = newMessages;
                    _groups.UpdateOne(x => x.GroupId == group.GroupId, Builders<Groups>.Update.Set(x => x.Chats, group.Chats));
                }
                else
                {
                  
                    _groups.UpdateOne(x => x.GroupId == group.GroupId, Builders<Groups>.Update.Push(x => x.Chats, _message));
                 
                }
               

                // group.Chats.ToList().ForEach(c => c.ReadBy.Add(myuserId));
                //  await _hubContext.Clients.Client(him.ConnectionId).SendAsync("SentMessage", message);
                //  await _hubContext.Clients.Client(me.ConnectionId).SendAsync("SentMessage", message);


            }
            return group;
        }
        public Groups FindGroup(string theiruserId,string myuserId)
        {
         
             var filter1 = Builders<Groups>.Filter.Eq(x => x.GroupName, "");
            var filter2 = Builders<Groups>.Filter.In(theiruserId,myuserId);
            var group = _groups.Find(x=>x.GroupName=="" && (x.Members.Contains(myuserId)) && x.Members.Contains(theiruserId)).FirstOrDefault();
            if (group != null)
            {
                //Update read count

                // group.Chats.ToList().ForEach(c => c.ReadBy.Add(myuserId));
                var chats = group.Chats.ToList();
                for (int i = 0; i < chats.Count(); i++)
                {
                    if (!chats[i].ReadBy.Contains(myuserId))
                    {
                        chats[i].ReadBy.Add(myuserId);
                    }
                }
                _groups.UpdateOne(x => x.GroupId == group.GroupId, Builders<Groups>.Update.Set(x => x.Chats, group.Chats));
               // var update = _groups.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.Status, status));
            }
            return group;
        }
        public Groups FindGroupChannel(string groupId, string myuserId)
        {


            var group = _groups.Find(x => x.GroupId == groupId).FirstOrDefault();
            if (group != null)
            {
                //Update read count

                // group.Chats.ToList().ForEach(c => c.ReadBy.Add(myuserId));

                
                if (group.Chats != null)
                {
                    var chats = group.Chats.ToList();
                    for (int i = 0; i < chats.Count(); i++)
                    {
                        if (!chats[i].ReadBy.Contains(myuserId))
                        {
                            chats[i].ReadBy.Add(myuserId);
                        }
                    }
                    _groups.UpdateOne(x => x.GroupId == group.GroupId, Builders<Groups>.Update.Set(x => x.Chats, group.Chats));
                }
                
                // var update = _groups.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.Status, status));
            }
            return group;
        }
        public async Task<Employee> SetStatus(string userId, string status)
        {
           
            var obj = new ClientObject();
            obj.userId = userId;
            obj.status = status;
            obj.lastseen = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");
            await _hubContext.Clients.All.SendAsync("setStatus", obj);
            if (status == "Online")
            {
              //  var updateSignalrID = _employees.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.ConnectionId, connectionId));
            }
            else
            {
                var updateSignalrID = _employees.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.ConnectionId, ""));
            }
           
            var _emp = _employees.UpdateOne(x => x.UserId ==userId, Builders<Employee>.Update.Set(x => x.Status,status));
            var __emp = _employees.UpdateOne(x => x.UserId == userId, Builders<Employee>.Update.Set(x => x.LastOnline, DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss")));
            var emp = _employees.Find(x => x.UserId == userId).FirstOrDefault();
            return emp;
        }

        public async Task<Groups> CreateChannel(string groupName, string groupDescription, string createdBy, string[] userIDs)
        {
            var group = new Groups();
            group.CreatedBy = createdBy;
            group.GroupDescription = groupDescription;
            group.GroupName = groupName;
            List<string> users = new List<string>();

            for (int i = 0; i < userIDs.Length; i++)
            {
                if(createdBy!= userIDs[i])
                {
                   
                    //await _hubContext.Clients.Client(user.ConnectionId).SendAsync("NewGroupNotification", group);
                    users.Add(userIDs[i]);
                }
                
            }
            users.Add(createdBy);
            group.Members = users;
            group.Modified = DateTime.Now;
            //var Createduser = _employees.Find(x => x.UserId == createdBy).FirstOrDefault();
            //await _hubContext.Clients.Client(Createduser.ConnectionId).SendAsync("NewGroupNotification", group);





            //List<string> connectionIds = new List<string>();
            //for (int i = 0; i < users.Count(); i++)
            //{
            //    var user = _employees.Find(x => x.UserId == userIDs[i]).FirstOrDefault();
            //    if (user.ConnectionId != "")
            //    {
            //        connectionIds.Add(user.ConnectionId);
            //        await _hubContext.Groups.AddToGroupAsync(user.ConnectionId, groupName);

            //    }

            //}


            // await _hubContext.Clients.Clients(connectionIds).SendAsync("NewGroupNotification", group);
            //await _hubContext.Clients.Group(groupName).SendAsync("NewGroupNotification", group);
           _groups.InsertOne(group);
            var groupInserted = _groups.Find(x => x.GroupName == group.GroupName && x.GroupDescription == group.GroupDescription).FirstOrDefault();
            await Task.WhenAll(users.Select((id) => DoWorkAsync(id, groupInserted)));

            return group;
        }

        private Task DoWorkAsync(string id, Groups group)
        {
            var user = _employees.Find(x => x.UserId == id).FirstOrDefault();
           
                return  _hubContext.Clients.Client(user.ConnectionId).SendAsync("NewGroupNotification", group);
          
           
        }

        private async Task<string> DoChannelMessageDeliveryAsync(string id, Groups group,Messages message)
        {

            var _ChannelMessage = new ChannelMessage();
            _ChannelMessage.group = group;
            _ChannelMessage.Message = message;
            var user = _employees.Find(x => x.UserId == id).FirstOrDefault();
            if (user.ConnectionId!="")
            {
                await _hubContext.Clients.Client(user.ConnectionId).SendAsync("NewChannelMessage", _ChannelMessage);
            }
               
            return user.ConnectionId;
            //else
            //{
            //    return null;
            //}
        }

        //private DoWorkAsync(string userID, Groups group)
        //{
        //    var user = _employees.Find(x => x.UserId == userID).FirstOrDefault();
        //    if (user.ConnectionId!="")
        //    {
        //        return await _hubContext.Clients.Client(user.ConnectionId).SendAsync("NewGroupNotification", group);
        //    }



        //}

        public async Task<Groups> CreateIndividualGroupAsync(string theiruserId, string myuserId, string mymessage, bool isNew,string connectionId, string groupid = "")
        {
            var message = new Messages();
            var me = _employees.Find(x => x.UserId == myuserId).FirstOrDefault();
            var him = _employees.Find(x => x.UserId == theiruserId).FirstOrDefault();
            if (isNew)
            {
                var group = new Groups();
                group.CreatedBy = myuserId;
                group.GroupDescription = "";
                group.GroupName = "";
                group.Modified = DateTime.Now;
                var members = new List<string>();
                members.Add(myuserId);
                members.Add(theiruserId);
                group.Members = members;
              
                message.Text = mymessage;
                message.UserId = myuserId;
                message.Modified = DateTime.Now;
                message.Avatar = me.Avatar;
                message.UserName = me.FirstName;
                var readbyuser = new List<string>();
                readbyuser.Add(myuserId);
                message.ReadBy = readbyuser;

                var messagelist = new List<Messages>();
                messagelist.Add(message);

                group.Chats = messagelist;

                await _hubContext.Clients.Client(him.ConnectionId).SendAsync("SentMessage", message);
                await _hubContext.Clients.Client(me.ConnectionId).SendAsync("SentMessage", message);

                _groups.InsertOne(group);


              
            }
            else
            {
               // var _groupresult = _groups.Find(x => x.GroupName == "" && (x.Members.Contains(myuserId)) && x.Members.Contains(theiruserId)).FirstOrDefault();
               
                message.Modified = DateTime.Now;

                var readbyuser = new List<string>();
                readbyuser.Add(myuserId);
                message.ReadBy = readbyuser;
                message.Text = mymessage;
                message.UserId = myuserId;
                message.Avatar = me.Avatar;
                message.UserName = me.FirstName;

                await _hubContext.Clients.Client(him.ConnectionId).SendAsync("SentMessage", message);
                await _hubContext.Clients.Client(me.ConnectionId).SendAsync("SentMessage", message);
                var filter = Builders<Groups>
             .Filter.Eq(e => e.GroupId, groupid);

                var update = Builders<Groups>.Update
                        .Push<Messages>(e => e.Chats, message);

               await  _groups.FindOneAndUpdateAsync(filter, update);
               
            }
          
            var groupresult = _groups.Find(x => x.GroupName == "" && (x.Members.Contains(myuserId)) && x.Members.Contains(theiruserId)).FirstOrDefault();

            foreach (var chat in groupresult.Chats)
            {
                var user= _employees.Find(emp => emp.UserId==chat.UserId).FirstOrDefault();
                chat.Avatar = user.Avatar;
                chat.UserName = user.FirstName;
            }
           

            return groupresult;
        }
        //public List<Employee> Get()
        //{
        //    List<Employee> employees;
        //    employees = _employees.Find(emp => true).ToList();
        //    return employees;
        //}
        public async Task<Employee> SetConnectionID(string userId, string connectionId)
        {
            //var filter = Builders<Employee>
            //.Filter.Eq(e => e.UserId, userId);

            //  var update = Builders<Employee>.Update.Set("ConnectionId", connectionId);

            //var _emp=  _employees.FindOneAndUpdate(filter, update);
            var filter = Builders<Employee>
              .Filter.Eq(e => e.UserId, userId);

            var update = Builders<Employee>.Update.Set<string>(e => e.ConnectionId, connectionId);
            //.Push<Messages>(e => e.Chats, message);

            await _employees.FindOneAndUpdateAsync(filter, update);

            var emp = _employees.Find(x => x.UserId == userId).FirstOrDefault();
            return emp;


        }

        public List<Employee> GetAllEmployees(string myUserId)
        {
            List<Employee> employees;
            employees = _employees.Find(new BsonDocument()).ToList();

            for (int i = 0; i < employees.Count; i++)
            {
                var unreadCount = 0;
                employees[i].badge = 0;
                var _groupresult =  _groups.Find(x => x.GroupName == "" && (x.Members.Contains(myUserId)) && x.Members.Contains(employees[i].UserId)).FirstOrDefault();
                if (_groupresult!=null)
                {
                  var chatsc=   _groupresult.Chats.FindAll((x => !x.ReadBy.Contains(myUserId)));
                    unreadCount= chatsc.Count();
                    employees[i].UnreadCount = unreadCount;
                }
            }
            return employees;
        }
        public List<Groups> GetAllGroups(string myUserId)
        {
            List<Groups> groups;
            groups = _groups.Find(x => x.GroupName != "" && (x.Members.Contains(myUserId))).ToList();

            for (int i = 0; i < groups.Count; i++)
            {
                var unreadCount = 0;
              
               
                if (groups[i].GroupName != "")
                {
                    if (groups[i].Chats != null)
                    {
                        var chatsc = groups[i].Chats.FindAll((x => !x.ReadBy.Contains(myUserId)));
                        unreadCount = chatsc.Count();
                        groups[i].UnreadCount = unreadCount;
                    }
                   
                }
            }

            return groups;
        }

        public Employee Login(string email, string password)
        {
           
            var employe = _employees.Find(x=>x.Email==email && x.PassWord==password).FirstOrDefault();
            return employe;
        }

        public List<Employee> CreateEmployees()
        {
            List<Employee> employees = new List<Employee>();
            employees.Add(new Employee() { FirstName = "Riyas", LastName = "KS", AbsenseNote = "None", Email = "riyasalim123@gmail.com", Groups = null, PassWord = "riyas", Phone = "+919746174508", Status = "Online" });

            string[] names = { "Fatih Temel", "Erik Pauli", "Alexander Kleinen", "Dennis Ollig", "Andreas S", "Philipp Scherber", "Biplov K.C", "Florian Bernd", "Maximilian Hertling", "Marina Rukavitsyna", "Stefan Bühler", "Ferenc Pato", "Balla Győző", "Varga Zsolt", "Alina Kochems", "Manuela Stobb", "Lukas Ginsberg", "Sandy Rothbrust", "Manuel Zimmer", "Syed Ahmad", "Malte Schweer" };

            for (int i = 0; i < names.Length; i++)
            {
                var firstName = names[i].Split(" ")[0];
                var lastName = names[i].Split(" ")[1];
                employees.Add(new Employee() { FirstName = firstName,Avatar= "https://previews.123rf.com/images/salamatik/salamatik1801/salamatik180100019/92979836-profile-anonymous-face-icon-gray-silhouette-person-male-default-avatar-photo-placeholder-isolated-on.jpg", LastName = lastName, AbsenseNote = "None", Email = firstName + lastName + "@dirico.com", Groups = null, PassWord = "dirico", Phone = "+919746174508", Status = "Offline",ConnectionId="" });
            }

            _employees.InsertMany(employees);
            var emp = _employees.Find(new BsonDocument()).ToList();
            return emp;
        }


    }
}
