﻿using diricowebapi.Models;
using diricowebapi.services;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    public class ClientObject
    {
        public string connectionId { get; set; }
        public string userId { get; set; }
        public string status { get; set; }
        public string lastseen { get; set; }
    } 
    public class ChatHub : Hub
    {
        private readonly EmployeeService _employeeService;
        public ChatHub(EmployeeService employeeService)
        {
            _employeeService = employeeService;

        }
        public async Task SendConnectionId(string connectionId,string userId,string status)
        {
            var obj = new ClientObject();
            obj.connectionId = connectionId;
            obj.userId = userId;
            obj.status = status;
            await _employeeService.SetConnectionID(userId, connectionId);
              await Clients.All.SendAsync("setStatus", obj );

        }
        public async Task SentMessage(Messages message,string connectionId)
        {
          
            await  Clients.Client(connectionId).SendAsync("SentMessage", message);

        }
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }
    }

}