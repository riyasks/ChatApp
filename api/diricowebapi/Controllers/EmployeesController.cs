﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using diricowebapi.Models;
using diricowebapi.services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SignalRChat.Hubs;


namespace diricowebapi.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly EmployeeService _employeeService;
        //IHubContext<ChatHub, ChatHub> _chatHubContext;
        private readonly IHubContext<ChatHub> _hubContext;
        public EmployeesController(EmployeeService employeeService)
        {
            _employeeService = employeeService;
          
        }
        
        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public IActionResult CreateEmployees()
        {
          
           var employyes= _employeeService.CreateEmployees();
            return Ok(employyes);
        }
       

        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult GetAllEmployees(string myUserID)
        {
            var emp = _employeeService.GetAllEmployees(myUserID);

            if (emp == null)
            {
                return NotFound();
            }

            return Ok(emp);
        }
        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult GetAllGroups(string myUserID)
        {
            var grp = _employeeService.GetAllGroups(myUserID);

            if (grp == null)
            {
                return NotFound();
            }

            return Ok(grp);
        }

        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult FindGroup(string theiruserId, string myuserId)
        {
            var group = _employeeService.FindGroup(theiruserId, myuserId);

            if (group == null)
            {
                return Ok("No Group");
            }

            return Ok(group);
        }
        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult FindGroupChannel(string groupId, string myuserId)
        {
            var group = _employeeService.FindGroupChannel(groupId, myuserId);

            if (group == null)
            {
                return Ok("No Group");
            }

            return Ok(group);
        }

        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult UpdateReadMessage(string userId, string groupId)
        {
            var group = _employeeService.UpdateReadMessage(userId, groupId);

            if (group == null)
            {
                return Ok("Group Not Updated ");
            }

            return Ok(group);
        }

        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public async Task<IActionResult> CreateChannel(string groupName, string groupDescription,string createdBy,[FromUri] string Selectedusers)
        {
            string[] userIDs = JsonConvert.DeserializeObject<string[]>(Selectedusers);
            var dat = await _employeeService.CreateChannel(groupName, groupDescription, createdBy, userIDs);

            return Ok(dat);
        }
        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public async Task<IActionResult> sendMessage(string mymessage, string myuserid,string theiruserId,string connectionId)
        {

            var _group = new Groups();
            var group = _employeeService.FindGroup(theiruserId, myuserid);

            if (group == null)
            {
                //Create group 
                 _group =await _employeeService.CreateIndividualGroupAsync(theiruserId, myuserid, mymessage,true,connectionId);
              
            }
            else
            {
                //update chat in the group

                 _group = await _employeeService.CreateIndividualGroupAsync(theiruserId, myuserid, mymessage, false, connectionId, group.GroupId);
            }
            //var group = _employeeService.FindGroup(theiruserId, myuserId);

            //if (group == null)
            //{
            //    return Ok("No Group");
            //}

            return Ok(_group);
        }
        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult sendChannelMessage(string mymessage, string myuserid, string selectchannelId)
        {

            //var _group = new Groups();
            var group = _employeeService.sendChannelMessageAsync(mymessage, myuserid, selectchannelId);


            return Ok(group);
        }


        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult Login(string email,string password)
        {
            var emp =  _employeeService.Login(email, password);

            if (emp == null)
            {
                return Ok("nouser");
            }
            else
            {
                return Ok(emp);
            }
            //var obj = new ClientObject();
            //obj.connectionId = "";
            //obj.userId = emp.UserId;
            //obj.status = "Online";
            //await _hubContext.Clients.All.SendAsync("setStatus", obj);
           
        }

        [Microsoft.AspNetCore.Mvc.Route("[action]")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        [EnableCors("AllowOrigin")]
        public async Task<IActionResult> SetStatus(string userId, string status)
        {
            var emp =await _employeeService.SetStatus(userId, status);

            if (emp == null)
            {
                return NotFound();
            }
         
            return Ok(emp);
        }
    }
}