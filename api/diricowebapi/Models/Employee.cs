﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diricowebapi.Models
{
    public class Employee
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
        public string Avatar { get; set; }
        public string PassWord { get; set; }
        public string Status { get; set; }
        public string AbsenseNote { get; set; }

        public string ConnectionId { get; set; }
        public string LastOnline { get; set; }
        public int badge { get; set; }
        public int UnreadCount { get; set; }
        public List<GroupViewModel> Groups { get; set; }
    }

    public class GroupViewModel
    {
        public Groups Group { get; set; }
        public DateTime Date { get; set; }
    }
    public class Groups
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string GroupId { get; set; }

        public string GroupName { get; set; }

        public List<string> Members { get; set; }

        public List<Messages> Chats { get; set; }

        public string GroupDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Modified { get; set; }
        public int UnreadCount { get; set; }
    }

    public class Messages
    {
       
        public string Text { get; set; }

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Avatar { get; set; }
        public List<string> ReadBy { get; set; }
        public DateTime Modified { get; set; }
    }
    public class User
    {
        public string id { get; set; }
        //public string name { get; set; }
    }
    public class Users
    {
        public List<User> Selectedusers { get; set; }
         
    }

    public class ChannelMessage
    {
        public Groups group { get; set; }
        public Messages Message { get; set; }
    }
}
